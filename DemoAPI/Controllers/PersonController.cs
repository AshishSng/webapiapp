﻿using DemoAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DemoAPI.Controllers
{
    public class PersonController : ApiController
    {
        List<PersonModel> people = new List<PersonModel>();
        public PersonController()
        {
            people.Add(new PersonModel { Id = 1, FirstName = "Ashish", LastName = "Singh" });
            people.Add(new PersonModel { Id = 2, FirstName = "Shane", LastName = "Pilon" });
            people.Add(new PersonModel { Id = 3, FirstName = "Craig", LastName = "Pitcher" });
        }
        [Route("api/person/GetFirstName")]
        public IEnumerable<string> GetFirstName()
        {
            List<string> output = new List<string>();
            foreach (var ppl in people)
            {
                output.Add(ppl.FirstName);
            }
            return output;

        }
        // GET: api/Person
        public IEnumerable<PersonModel> Get()
        {
            return people;
        }

        // GET: api/Person/5
        public PersonModel Get(int id)
        {
            return people.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST: api/Person
        public void Post(PersonModel value)
        {
            people.Add(value);
        }

        // PUT: api/Person/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Person/5
        public void Delete(int id)
        {
        }
    }
}
