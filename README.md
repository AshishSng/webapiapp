# WebAPIApp

This project demonstrate Creating and WebAPI from scratch.

DemoAPI has PersonModel and PersonController. PersonModel has basically three peroperties - Id, FirstName, and LastName.
PersonController inherits from ApiController. PersonController has Get(), Get(int id), Post(PersonModel val), and GetFirstName() methods. This demo also shows DataAnnotations such as [Route] .